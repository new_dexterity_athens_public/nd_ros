#include <nd_ros_hardware_interface/ros_hardware_interface.hpp>
#include <chrono>
#include <cmath>
#include <std_msgs/String.h>
#include <std_msgs/UInt8.h>


namespace ndx_dynx = new_dexterity::dynamixel_protocol;

namespace new_dexterity
{
    namespace utils 
    {
        constexpr auto THUMB_COMPENSATION = 40;

        constexpr double degrees_to_rads(double degrees)
        {
            return degrees * 0.0174533;
        }

        constexpr auto DYNX_POSITION_TO_RADS = 0.0015358904;
        constexpr auto DYNX_VELOCITY_TO_RPS = 0.105; 
        constexpr auto DYNX_CURRENT_TO_MA = 2.69;

        constexpr double dynx_position_to_rads(std::int32_t pos)
        {
            return pos * DYNX_POSITION_TO_RADS;
        }

        constexpr std::int32_t rads_to_dynx_position(double rads)
        {
            return static_cast<std::int32_t>(rads / DYNX_POSITION_TO_RADS);
        }

        constexpr double dynx_velocity_to_rps(std::int32_t vel)
        {
            return vel * DYNX_VELOCITY_TO_RPS;
        }

        constexpr std::uint32_t rps_to_dynx_velocity_limit(double vel)
        {
            return static_cast<std::uint32_t>(vel / DYNX_VELOCITY_TO_RPS);
        }

        constexpr double dynx_current_to_ma(std::int16_t cur)
        {
            return cur * DYNX_CURRENT_TO_MA;
        }

        constexpr std::int16_t ma_to_dynx_current(double ma)
        {
            return static_cast<std::int16_t>(ma / DYNX_CURRENT_TO_MA);
        }

        void enforce_multiplier(double& joint_pos, double& motor_pos, double motor_start, double motor_end, double joint_start, double joint_end)
        {
                if (motor_pos >= motor_start && motor_pos <= motor_end)
                {
                    joint_pos = joint_start + (joint_end - joint_start)/(motor_end - motor_start) * (motor_pos - motor_start);
                }
        }

    }

    ros_hardware_interface::ros_hardware_interface(ros::NodeHandle& node_handle, ndx_dynx::out_connection& out_connection
                                                           , ndx_dynx::communication_interface& thumb_opposition_interface
                                                           , ndx_dynx::communication_interface& thumb_interface
                                                           , ndx_dynx::communication_interface& index_interface
                                                           , ndx_dynx::communication_interface& middle_interface
                                                           , ndx_dynx::communication_interface& ring_interface
                                                           , ndx_dynx::communication_interface& pinky_interface)
        : node_handle(node_handle)
        , out_connection(out_connection)
        , thumb_opposition_interface(thumb_opposition_interface)
        , thumb_interface(thumb_interface)
        , index_interface(index_interface)
        , middle_interface(middle_interface)
        , ring_interface(ring_interface)
        , pinky_interface(pinky_interface)
        , pos() , vel(), eff()
        , cmd_pos(), cmd_vel(), cmd_eff()
        {
            hardware_interface::JointStateHandle state_handle_thumb_opposition("thumb_opposition_motor", &pos[joints::THUMB_OPPOSITION_MOTOR], &vel[joints::THUMB_OPPOSITION_MOTOR], &eff[joints::THUMB_OPPOSITION_MOTOR]);
            joint_state_interface.registerHandle(state_handle_thumb_opposition);

            hardware_interface::JointStateHandle state_handle_thumb_motor("thumb_motor", &pos[joints::THUMB_MOTOR], &vel[joints::THUMB_MOTOR], &eff[joints::THUMB_MOTOR]);
            joint_state_interface.registerHandle(state_handle_thumb_motor);

            hardware_interface::JointStateHandle state_handle_index_motor("index_motor", &pos[joints::INDEX_MOTOR], &vel[joints::INDEX_MOTOR], &eff[joints::INDEX_MOTOR]);
            joint_state_interface.registerHandle(state_handle_index_motor);

            hardware_interface::JointStateHandle state_handle_middle_motor("middle_motor", &pos[joints::MIDDLE_MOTOR], &vel[joints::MIDDLE_MOTOR], &eff[joints::MIDDLE_MOTOR]);
            joint_state_interface.registerHandle(state_handle_middle_motor);

            hardware_interface::JointStateHandle state_handle_ring_motor("ring_motor", &pos[joints::RING_MOTOR], &vel[joints::RING_MOTOR], &eff[joints::RING_MOTOR]);
            joint_state_interface.registerHandle(state_handle_ring_motor);

            hardware_interface::JointStateHandle state_handle_pinky_motor("pinky_motor", &pos[joints::PINKY_MOTOR], &vel[joints::PINKY_MOTOR], &eff[joints::PINKY_MOTOR]);
            joint_state_interface.registerHandle(state_handle_pinky_motor);


            hardware_interface::JointStateHandle state_handle_thumb_proximal("thumb_proximal", &pos[joints::THUMB_PROXIMAL], &vel[joints::THUMB_PROXIMAL], &eff[joints::THUMB_PROXIMAL]);
            joint_state_interface.registerHandle(state_handle_thumb_proximal);

            hardware_interface::JointStateHandle state_handle_thumb_distal("thumb_distal", &pos[joints::THUMB_DISTAL], &vel[joints::THUMB_DISTAL], &eff[joints::THUMB_DISTAL]);
            joint_state_interface.registerHandle(state_handle_thumb_distal);

            hardware_interface::JointStateHandle state_handle_index_proximal("index_proximal", &pos[joints::INDEX_PROXIMAL], &vel[joints::INDEX_PROXIMAL], &eff[joints::INDEX_PROXIMAL]);
            joint_state_interface.registerHandle(state_handle_index_proximal);


            hardware_interface::JointStateHandle state_handle_index_distal("index_distal", &pos[joints::INDEX_DISTAL], &vel[joints::INDEX_DISTAL], &eff[joints::INDEX_DISTAL]);
            joint_state_interface.registerHandle(state_handle_index_distal);

            hardware_interface::JointStateHandle state_handle_middle_proximal("middle_proximal", &pos[joints::MIDDLE_PROXIMAL], &vel[joints::MIDDLE_PROXIMAL], &eff[joints::MIDDLE_PROXIMAL]);
            joint_state_interface.registerHandle(state_handle_middle_proximal);


            hardware_interface::JointStateHandle state_handle_middle_distal("middle_distal", &pos[joints::MIDDLE_DISTAL], &vel[joints::MIDDLE_DISTAL], &eff[joints::MIDDLE_DISTAL]);
            joint_state_interface.registerHandle(state_handle_middle_distal);

            hardware_interface::JointStateHandle state_handle_ring_proximal("ring_proximal", &pos[joints::RING_PROXIMAL], &vel[joints::RING_PROXIMAL], &eff[joints::RING_PROXIMAL]);
            joint_state_interface.registerHandle(state_handle_ring_proximal);

            hardware_interface::JointStateHandle state_handle_ring_distal("ring_distal", &pos[joints::RING_DISTAL], &vel[joints::RING_DISTAL], &eff[joints::RING_DISTAL]);
            joint_state_interface.registerHandle(state_handle_ring_distal);

            hardware_interface::JointStateHandle state_handle_pinky_proximal("pinky_proximal", &pos[joints::PINKY_PROXIMAL], &vel[joints::PINKY_PROXIMAL], &eff[joints::PINKY_PROXIMAL]);
            joint_state_interface.registerHandle(state_handle_pinky_proximal);

            hardware_interface::JointStateHandle state_handle_pinky_distal("pinky_distal", &pos[joints::PINKY_DISTAL], &vel[joints::PINKY_DISTAL], &eff[joints::PINKY_DISTAL]);
            joint_state_interface.registerHandle(state_handle_pinky_distal);

            registerInterface(&joint_state_interface);


            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_thumb_opposition(joint_state_interface.getHandle("thumb_opposition_motor"), &cmd_pos[joints::THUMB_OPPOSITION_MOTOR], &cmd_vel[joints::THUMB_OPPOSITION_MOTOR], &cmd_eff[joints::THUMB_OPPOSITION_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_thumb_opposition);

            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_thumb_motor(joint_state_interface.getHandle("thumb_motor"), &cmd_pos[joints::THUMB_MOTOR], &cmd_vel[joints::THUMB_MOTOR], &cmd_eff[joints::THUMB_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_thumb_motor);

            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_index_motor(joint_state_interface.getHandle("index_motor"), &cmd_pos[joints::INDEX_MOTOR], &cmd_vel[joints::INDEX_MOTOR], &cmd_eff[joints::INDEX_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_index_motor);

            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_middle_motor(joint_state_interface.getHandle("middle_motor"), &cmd_pos[joints::MIDDLE_MOTOR], &cmd_vel[joints::MIDDLE_MOTOR], &cmd_eff[joints::MIDDLE_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_middle_motor);

            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_ring_motor(joint_state_interface.getHandle("ring_motor"), &cmd_pos[joints::RING_MOTOR], &cmd_vel[joints::RING_MOTOR], &cmd_eff[joints::RING_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_ring_motor);

            nd_hardware_interface::PosVelEffJointHandle pos_vel_eff_handle_pinky_motor(joint_state_interface.getHandle("pinky_motor"), &cmd_pos[joints::PINKY_MOTOR], &cmd_vel[joints::PINKY_MOTOR], &cmd_eff[joints::PINKY_MOTOR]);
            pos_vel_eff_joint_interface.registerHandle(pos_vel_eff_handle_pinky_motor);

            registerInterface(&pos_vel_eff_joint_interface);

            thumb_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/thumb_hardware_status", 1000);
            thumb_opposition_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/thumb_opposition_hardware_status", 1000);
            index_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/index_hardware_status", 1000);
            middle_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/middle_hardware_status", 1000);
            ring_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/ring_hardware_status", 1000);
            pinky_hardware_status_publisher = node_handle.advertise<std_msgs::String>("hardware_interface/pinky_hardware_status", 1000);

            thumb_reboot_service = node_handle.advertiseService("hardware_interface/reboot_thumb", &ros_hardware_interface::reboot_thumb, this);
            thumb_opposition_reboot_service = node_handle.advertiseService("hardware_interface/reboot_thumb_opposition", &ros_hardware_interface::reboot_thumb_opposition, this);
            index_reboot_service = node_handle.advertiseService("hardware_interface/reboot_index", &ros_hardware_interface::reboot_index, this);
            middle_reboot_service = node_handle.advertiseService("hardware_interface/reboot_middle", &ros_hardware_interface::reboot_middle, this);
            ring_reboot_service = node_handle.advertiseService("hardware_interface/reboot_ring", &ros_hardware_interface::reboot_ring, this);
            pinky_reboot_service = node_handle.advertiseService("hardware_interface/reboot_pinky", &ros_hardware_interface::reboot_pinky, this);
            all_reboot_service = node_handle.advertiseService("hardware_interface/reboot_all", &ros_hardware_interface::reboot_all, this);

            thumb_enable_service = node_handle.advertiseService("hardware_interface/enable_thumb", &ros_hardware_interface::enable_thumb, this);
            thumb_opposition_enable_service = node_handle.advertiseService("hardware_interface/enable_thumb_opposition", &ros_hardware_interface::enable_thumb_opposition, this);
            index_enable_service = node_handle.advertiseService("hardware_interface/enable_index", &ros_hardware_interface::enable_index, this);
            middle_enable_service = node_handle.advertiseService("hardware_interface/enable_middle", &ros_hardware_interface::enable_middle, this);
            ring_enable_service = node_handle.advertiseService("hardware_interface/enable_ring", &ros_hardware_interface::enable_ring, this);
            pinky_enable_service = node_handle.advertiseService("hardware_interface/enable_pinky", &ros_hardware_interface::enable_pinky, this);
            all_enable_service = node_handle.advertiseService("hardware_interface/enable_all", &ros_hardware_interface::enable_all, this);

            thumb_disable_service = node_handle.advertiseService("hardware_interface/disable_thumb", &ros_hardware_interface::disable_thumb, this);
            thumb_opposition_disable_service = node_handle.advertiseService("hardware_interface/disable_thumb_opposition", &ros_hardware_interface::disable_thumb_opposition, this);
            index_disable_service = node_handle.advertiseService("hardware_interface/disable_index", &ros_hardware_interface::disable_index, this);
            middle_disable_service = node_handle.advertiseService("hardware_interface/disable_middle", &ros_hardware_interface::disable_middle, this);
            ring_disable_service = node_handle.advertiseService("hardware_interface/disable_ring", &ros_hardware_interface::disable_ring, this);
            pinky_disable_service = node_handle.advertiseService("hardware_interface/disable_pinky", &ros_hardware_interface::disable_pinky, this);
            all_disable_service = node_handle.advertiseService("hardware_interface/disable_all", &ros_hardware_interface::disable_all, this);

            thumb_zero_service = node_handle.advertiseService("hardware_interface/zero_thumb", &ros_hardware_interface::zero_thumb, this);
            thumb_opposition_zero_service = node_handle.advertiseService("hardware_interface/zero_thumb_opposition", &ros_hardware_interface::zero_thumb_opposition, this);
            index_zero_service = node_handle.advertiseService("hardware_interface/zero_index", &ros_hardware_interface::zero_index, this);
            middle_zero_service = node_handle.advertiseService("hardware_interface/zero_middle", &ros_hardware_interface::zero_middle, this);
            ring_zero_service = node_handle.advertiseService("hardware_interface/zero_ring", &ros_hardware_interface::zero_ring, this);
            pinky_zero_service = node_handle.advertiseService("hardware_interface/zero_pinky", &ros_hardware_interface::zero_pinky, this);
            all_zero_service = node_handle.advertiseService("hardware_interface/zero_all", &ros_hardware_interface::zero_all, this);

            thumb_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/thumb_temperature", 1000);
            thumb_opposition_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/thumb_opposition_temperature", 1000);
            index_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/index_temperature", 1000);
            middle_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/middle_temperature", 1000);
            ring_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/ring_temperature", 1000);
            pinky_temperature_publisher = node_handle.advertise<std_msgs::UInt8>("hardware_interface/pinky_temperature", 1000);

            //disable torque for configuration
            auto r1 = thumb_opposition_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));
            auto r2 = thumb_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));
            auto r3 = index_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));
            auto r4 = middle_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));
            auto r5 = ring_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));
            auto r6 = pinky_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{false}),
                        std::chrono::milliseconds(200));

            //set operating mode to 5 - position based torque control
            auto r7 = thumb_opposition_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));
            auto r8 = thumb_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));
            auto r9 = index_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));
            auto r10 = middle_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));
            auto r11 = ring_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));
            auto r12 = pinky_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::operating_mode{5}),
                        std::chrono::milliseconds(200));

            //change direction of thumb and thumb opposition interfaces
            auto r13 = thumb_opposition_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{1}),
                        std::chrono::milliseconds(200));
            auto r14 = thumb_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{1}),
                            std::chrono::milliseconds(200));
            auto r15 = index_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{0}),
                        std::chrono::milliseconds(200));
            auto r16 = middle_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{0}),
                            std::chrono::milliseconds(200));
            auto r17 = ring_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{0}),
                        std::chrono::milliseconds(200));
            auto r18 = pinky_interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::drive_mode{0}),
                            std::chrono::milliseconds(200));
        }

        void ros_hardware_interface::report_hardware_error_status(ndx_dynx::communication_interface& interface, ros::Publisher& publisher, std::string finger_name)
        {
            auto mresponse = interface.send_request( 
                        ndx_dynx::get_memory_content_request<ndx_dynx::memory_handles::hardware_error_status>{},
                        std::chrono::milliseconds(200)
                        );

            if (!mresponse)
            {
                ROS_ERROR_STREAM(finger_name << " actuator: response timeout on hardware error status request");
                return;
            }

            auto response = *mresponse;

            return new_dexterity::match(response.result,
                    [&](ndx_dynx::instruction_error error)
                    {
                        ROS_ERROR_STREAM(finger_name << " actuator: got instruction error on hardware error status request - " << error);
                    },
                    [&](ndx_dynx::memory_content<ndx_dynx::memory_handles::hardware_error_status> const & content)
                    {
                        std::string error_str = "unknown error";

                        if (content.hardware_error_status & (1 << 0))
                        {
                            error_str = "input voltage error";
                        }
                        else if (content.hardware_error_status & (1 << 2))
                        {
                            error_str = "overheating error";
                        }
                        else if (content.hardware_error_status & (1 << 3))
                        {
                            error_str = "motor encoder error";
                        }
                        else if (content.hardware_error_status & (1 << 4))
                        {
                            error_str = "electrical shock error";
                        }
                        else if (content.hardware_error_status & (1 << 5))
                        {
                            error_str = "overload error";
                        }

                        ROS_WARN_STREAM(finger_name << " actuator: detected hardware error - " << error_str);

                        std_msgs::String msg;
                        msg.data = error_str;
                        publisher.publish(msg);
                    });
        }

	bool ros_hardware_interface::read_info(ndx_dynx::communication_interface& interface, boost::optional<ndx_dynx::info_response> mresponse, ros::Publisher& hardware_status_publisher, ros::Publisher temperature_publisher,
                       std::string finger_name, double& position_feedback
	                                      , double& velocity_feedback
	                                      , double& torque_feedback)
	{

		if (!mresponse)
                {
                    ROS_ERROR_STREAM(finger_name << " actuator: response timeout on read info request");
                    return false;
                }

		auto response = *mresponse;

                if (response.hardware_error_alert)
                {
                    report_hardware_error_status(interface, hardware_status_publisher, finger_name);
                    return false;
                }
                else
                {
                    std_msgs::String msg;
                    msg.data = "no error";
                    hardware_status_publisher.publish(msg);
                }

		return new_dexterity::match<bool>(response.result,
			[&](ndx_dynx::info const & info)
			{
				torque_feedback = utils::dynx_current_to_ma(info.current);
				velocity_feedback = utils::dynx_velocity_to_rps(info.velocity);
				position_feedback = utils::dynx_position_to_rads(info.position);

                                std_msgs::UInt8 msg;
                                msg.data = info.temperature;
                                temperature_publisher.publish(msg);
                                return true;
			},
			[&](ndx_dynx::instruction_error const & error)
			{
                                ROS_ERROR_STREAM(finger_name << " actuator: got instruction error on read info request" << error);
                                return false;
			});
	}

	bool ros_hardware_interface::read()
	{
                std::lock_guard<std::mutex> lock(mutex);
                out_connection.access([](std::vector<std::uint8_t>& buffer)
                {
                    ndx_dynx::sync_read<ndx_dynx::info_request> sync_read;
                    sync_read.ids.push_back(1);
                    sync_read.ids.push_back(2);
                    sync_read.ids.push_back(3);
                    sync_read.ids.push_back(4);
                    sync_read.ids.push_back(5);
                    sync_read.ids.push_back(6);

                    ndx_dynx::serialize(buffer, sync_read);
                });

                auto fut_mthumb_opposition_info_response = thumb_opposition_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));
                auto fut_mthumb_info_response = thumb_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));
                auto fut_mindex_info_response = index_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));
                auto fut_mmiddle_info_response = middle_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));
                auto fut_mring_info_response = ring_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));
                auto fut_mpinky_info_response = pinky_interface.wait_response_of<ndx_dynx::info_request>(std::chrono::milliseconds(100));

                out_connection.send_bytes();

                auto mpinky_info_response = fut_mpinky_info_response.get();
                auto mring_info_response = fut_mring_info_response.get();
                auto mmiddle_info_response = fut_mmiddle_info_response.get();
                auto mindex_info_response = fut_mindex_info_response.get();
                auto mthumb_info_response = fut_mthumb_info_response.get();
                auto mthumb_opposition_info_response = fut_mthumb_opposition_info_response.get();

		double position_feedback;
	    	double velocity_feedback;
	    	double torque_feedback;

		auto read_thumb_opposition = 
                    read_info(thumb_opposition_interface, mthumb_opposition_info_response, thumb_opposition_hardware_status_publisher, thumb_opposition_temperature_publisher, 
                               "thumb_opposition", position_feedback, velocity_feedback, torque_feedback);

                if (read_thumb_opposition)
                {
                    pos[joints::THUMB_OPPOSITION_MOTOR] = position_feedback;
                    vel[joints::THUMB_OPPOSITION_MOTOR] = velocity_feedback;
                    eff[joints::THUMB_OPPOSITION_MOTOR] = torque_feedback;
                }

		auto read_thumb = 
                    read_info(thumb_interface, mthumb_info_response, thumb_hardware_status_publisher, thumb_temperature_publisher, 
                               "thumb", position_feedback, velocity_feedback, torque_feedback);

                if (read_thumb && read_thumb_opposition)
                {
                    pos[joints::THUMB_MOTOR] = position_feedback - utils::degrees_to_rads(utils::THUMB_COMPENSATION) * std::min(1.0, pos[joints::THUMB_OPPOSITION_MOTOR] / utils::degrees_to_rads(90));
                    vel[joints::THUMB_MOTOR] = velocity_feedback;
                    eff[joints::THUMB_MOTOR] = torque_feedback;
                }

		auto read_index = 
                    read_info(index_interface, mindex_info_response, index_hardware_status_publisher, index_temperature_publisher, 
                               "index", position_feedback, velocity_feedback, torque_feedback);

                if (read_index)
                {
                    pos[joints::INDEX_MOTOR] = position_feedback;
                    vel[joints::INDEX_MOTOR] = velocity_feedback;
                    eff[joints::INDEX_MOTOR] = torque_feedback;
                }

		auto read_middle = 
                    read_info(middle_interface, mmiddle_info_response, middle_hardware_status_publisher, middle_temperature_publisher, 
                               "middle", position_feedback, velocity_feedback, torque_feedback);

                if (read_middle)
                {
                    pos[joints::MIDDLE_MOTOR] = position_feedback;
                    vel[joints::MIDDLE_MOTOR] = velocity_feedback;
                    eff[joints::MIDDLE_MOTOR] = torque_feedback;
                }

		auto read_ring = 
                    read_info(ring_interface, mring_info_response, ring_hardware_status_publisher, ring_temperature_publisher, 
                               "ring", position_feedback, velocity_feedback, torque_feedback);

                if (read_ring)
                {
                    pos[joints::RING_MOTOR] = position_feedback;
                    vel[joints::RING_MOTOR] = velocity_feedback;
                    eff[joints::RING_MOTOR] = torque_feedback;
                }

		auto read_pinky = 
                    read_info(pinky_interface, mpinky_info_response, pinky_hardware_status_publisher, pinky_temperature_publisher, 
                               "pinky", position_feedback, velocity_feedback, torque_feedback);

                if (read_pinky)
                {
                    pos[joints::PINKY_MOTOR] = position_feedback;
                    vel[joints::PINKY_MOTOR] = velocity_feedback;
                    eff[joints::PINKY_MOTOR] = torque_feedback;
                }

                if (!read_thumb_opposition || !read_thumb || !read_index || !read_middle || !read_ring || !read_pinky)
                    return false;

                utils::enforce_multiplier(
                        pos[joints::THUMB_PROXIMAL], pos[joints::THUMB_MOTOR],
                        utils::degrees_to_rads(0), utils::degrees_to_rads(60),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(60));

                utils::enforce_multiplier(
                        pos[joints::THUMB_DISTAL], pos[joints::THUMB_MOTOR],
                        utils::degrees_to_rads(60), utils::degrees_to_rads(165),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80));

                utils::enforce_multiplier(
                        pos[joints::INDEX_PROXIMAL], pos[joints::INDEX_MOTOR],
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80));

                utils::enforce_multiplier(
                        pos[joints::INDEX_PROXIMAL], pos[joints::INDEX_MOTOR],
                        utils::degrees_to_rads(80), utils::degrees_to_rads(207),
                        utils::degrees_to_rads(80), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::INDEX_DISTAL], pos[joints::INDEX_MOTOR],
                        utils::degrees_to_rads(70), utils::degrees_to_rads(213),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::MIDDLE_PROXIMAL], pos[joints::MIDDLE_MOTOR],
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80));

                utils::enforce_multiplier(
                        pos[joints::MIDDLE_PROXIMAL], pos[joints::MIDDLE_MOTOR],
                        utils::degrees_to_rads(80), utils::degrees_to_rads(207),
                        utils::degrees_to_rads(80), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::MIDDLE_DISTAL], pos[joints::MIDDLE_MOTOR],
                        utils::degrees_to_rads(70), utils::degrees_to_rads(213),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::RING_PROXIMAL], pos[joints::RING_MOTOR],
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80));

                utils::enforce_multiplier(
                        pos[joints::RING_PROXIMAL], pos[joints::RING_MOTOR],
                        utils::degrees_to_rads(80), utils::degrees_to_rads(207),
                        utils::degrees_to_rads(80), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::RING_DISTAL], pos[joints::RING_MOTOR],
                        utils::degrees_to_rads(70), utils::degrees_to_rads(213),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::PINKY_PROXIMAL], pos[joints::PINKY_MOTOR],
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(80));

                utils::enforce_multiplier(
                        pos[joints::PINKY_PROXIMAL], pos[joints::PINKY_MOTOR],
                        utils::degrees_to_rads(80), utils::degrees_to_rads(207),
                        utils::degrees_to_rads(80), utils::degrees_to_rads(90));

                utils::enforce_multiplier(
                        pos[joints::PINKY_DISTAL], pos[joints::PINKY_MOTOR],
                        utils::degrees_to_rads(70), utils::degrees_to_rads(213),
                        utils::degrees_to_rads(0), utils::degrees_to_rads(90));


		return true;
	}

	void ros_hardware_interface::prepare_cmd(ndx_dynx::sync_write<ndx_dynx::command_request>& sync_write, uint8_t id, std::string finger_name, double goal_position, double goal_velocity, double goal_torque)
	{
                ndx_dynx::command_request request;
                request.position = utils::rads_to_dynx_position(goal_position);
                request.velocity_limit = std::max(200u, utils::rps_to_dynx_velocity_limit(std::abs(goal_velocity)));

                request.current_limit = utils::ma_to_dynx_current(goal_torque);

                sync_write.requests.emplace_back(id, request);
	}

	void ros_hardware_interface::write()
	{
                std::lock_guard<std::mutex> lock(mutex);

                ndx_dynx::sync_write<ndx_dynx::command_request> sync_write;

		prepare_cmd(sync_write, 1, "thumb_opposition", std::min(utils::degrees_to_rads(90), cmd_pos[joints::THUMB_OPPOSITION_MOTOR]), cmd_vel[joints::THUMB_OPPOSITION_MOTOR], cmd_eff[joints::THUMB_OPPOSITION_MOTOR]);

		prepare_cmd(sync_write, 2, "thumb", 
                        cmd_pos[joints::THUMB_MOTOR] + 
                            utils::degrees_to_rads(utils::THUMB_COMPENSATION) * std::min(1.0, pos[joints::THUMB_OPPOSITION_MOTOR] / utils::degrees_to_rads(90)),
                        cmd_vel[joints::THUMB_MOTOR], 
                        cmd_eff[joints::THUMB_MOTOR]);

		prepare_cmd(sync_write, 3, "index", cmd_pos[joints::INDEX_MOTOR], cmd_vel[joints::INDEX_MOTOR], cmd_eff[joints::INDEX_MOTOR]);
		prepare_cmd(sync_write, 4, "middle", cmd_pos[joints::MIDDLE_MOTOR], cmd_vel[joints::MIDDLE_MOTOR], cmd_eff[joints::MIDDLE_MOTOR]);
		prepare_cmd(sync_write, 5, "ring", cmd_pos[joints::RING_MOTOR], cmd_vel[joints::RING_MOTOR], cmd_eff[joints::RING_MOTOR]);
		prepare_cmd(sync_write, 6, "pinky", cmd_pos[joints::PINKY_MOTOR], cmd_vel[joints::PINKY_MOTOR], cmd_eff[joints::PINKY_MOTOR]);

                out_connection.access([&](std::vector<std::uint8_t>& buffer)
                {
                    ndx_dynx::serialize(buffer, sync_write);
                });

                out_connection.send_bytes();

                std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}


        bool ros_hardware_interface::enable_dynamixel(ndx_dynx::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response)
        {
            auto mresponse = interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{ true }), std::chrono::milliseconds(200));

            if (!mresponse)
            {
                std::string msg = finger_name + " actuator: timeout on enable request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response = *mresponse;

            new_dexterity::match(response.result,
                    [&](ndx_dynx::instruction_acknowledged)
                    {
                        std::string msg = finger_name + " actuator: enabled";
                        ROS_INFO_STREAM(msg);
                        srv_response.message = msg;
                        srv_response.success = true;
                    },
                    [&](ndx_dynx::instruction_error err)
                    {
                        std::string msg = finger_name + " actuator: instruction error on enable request";
                        ROS_ERROR_STREAM(msg << " - " << err);
                        srv_response.message = msg;
                        srv_response.success = false;
                    });

            return true;

        }

        bool ros_hardware_interface::disable_dynamixel(ndx_dynx::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response)
        {
            auto mresponse = interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::torque_enable{ false }), std::chrono::milliseconds(200));

            if (!mresponse)
            {
                std::string msg = finger_name + " actuator: timeout on disable request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response = *mresponse;

            new_dexterity::match(response.result,
                    [&](ndx_dynx::instruction_acknowledged)
                    {
                        std::string msg = finger_name + " actuator: disabled";
                        ROS_INFO_STREAM(msg);
                        srv_response.message = msg;
                        srv_response.success = true;
                    },
                    [&](ndx_dynx::instruction_error err)
                    {
                        std::string msg = finger_name + " actuator: instruction error on disable request";
                        ROS_ERROR_STREAM(msg << " - " << err);
                        srv_response.message = msg;
                        srv_response.success = false;
                    });

            return true;

        }

        bool ros_hardware_interface::zero_dynamixel(ndx_dynx::communication_interface& interface, std::string finger_name, bool is_inverse,
                std_srvs::Trigger::Request const & rq, std_srvs::Trigger::Response & srv_response)
        {
            reboot_dynamixel(interface, finger_name, rq, srv_response);
            if (!srv_response.success) return true;

            auto mresponse0 = interface.send_request(ndx_dynx::get_memory_content_request<ndx_dynx::memory_handles::homing_offset>{},
                    std::chrono::milliseconds(200));

            if (!mresponse0)
            {
                std::string msg = finger_name + " actuator: timeout on offset request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response0 = *mresponse0;

            auto mcurrent_offset = new_dexterity::match<boost::optional<std::int32_t>>(response0.result,
                        [&](ndx_dynx::memory_content<ndx_dynx::memory_handles::homing_offset> const & content)
                        {
                            ROS_INFO_STREAM(finger_name << " actuator: got offset");
                            return content.homing_offset;
                        },
                        [&](ndx_dynx::instruction_error err)
                        {
                            std::string msg = finger_name + " actuator: instruction error on offset request";
                            ROS_ERROR_STREAM(msg << " - " << err);
                            srv_response.message = msg;
                            srv_response.success = false;
                            return boost::none;
                        });

            if (!mcurrent_offset)
                return false;

            auto current_offset = *mcurrent_offset;

            auto mresponse1 = interface.send_request(ndx_dynx::get_memory_content_request<ndx_dynx::memory_handles::present_position>{},
                    std::chrono::milliseconds(200));

            if (!mresponse1)
            {
                std::string msg = finger_name + " actuator: timeout on position request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response1 = *mresponse1;

            auto mposition = new_dexterity::match<boost::optional<std::int32_t>>(response1.result,
                        [&](ndx_dynx::memory_content<ndx_dynx::memory_handles::present_position> const & content)
                        {
                            ROS_INFO_STREAM(finger_name << " actuator: got position");
                            return content.present_position;
                        },
                        [&](ndx_dynx::instruction_error err)
                        {
                            std::string msg = finger_name + " actuator: instruction error on position request";
                            ROS_ERROR_STREAM(msg << " - " << err);
                            srv_response.message = msg;
                            srv_response.success = false;
                            return boost::none;
                        });

            if (!mposition)
                return false;

            auto position = *mposition;

            std::int32_t real_position = 0;
            
            if (is_inverse)
                real_position = -position - current_offset;
            else
                real_position = position - current_offset;

            auto new_offset = -real_position;

            auto mresponse2 = interface.send_request(ndx_dynx::make_set_memory_content_request(ndx_dynx::memory_handles::homing_offset{new_offset}), 
                    std::chrono::milliseconds(500));

            if (!mresponse2)
            {
                std::string msg = finger_name + " actuator: timeout on zero request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response2 = *mresponse2;

            new_dexterity::match(response2.result,
                    [&](ndx_dynx::instruction_acknowledged)
                    {
                        std::string msg = finger_name + " actuator: zeroed";
                        ROS_INFO_STREAM(msg);
                        srv_response.message = msg;
                        srv_response.success = true;
                    },
                    [&](ndx_dynx::instruction_error err)
                    {
                        std::string msg = finger_name + " actuator: instruction error on zero request";
                        ROS_ERROR_STREAM(msg << " - " << err);
                        srv_response.message = msg;
                        srv_response.success = false;
                    });

            return true;
        }

        bool ros_hardware_interface::reboot_dynamixel(ndx_dynx::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response)
        {
            auto mresponse = interface.send_request(ndx_dynx::reboot_request{}, std::chrono::milliseconds(200));

            if (!mresponse)
            {
                std::string msg = finger_name + " actuator: timeout on reboot request";
                ROS_ERROR_STREAM(msg);
                
                srv_response.message = msg;
                srv_response.success = false;
                return true;
            }

            auto response = *mresponse;

            new_dexterity::match(response.result,
                    [&](ndx_dynx::instruction_acknowledged)
                    {
                        std::string msg = finger_name + " actuator: rebooting";
                        std::this_thread::sleep_for(std::chrono::milliseconds(500));
                        ROS_INFO_STREAM(msg);
                        srv_response.message = msg;
                        srv_response.success = true;
                    },
                    [&](ndx_dynx::instruction_error err)
                    {
                        std::string msg = finger_name + " actuator: instruction error on reboot request";
                        ROS_ERROR_STREAM(msg << " - " << err);
                        srv_response.message = msg;
                        srv_response.success = false;
                    });

            return true;
        }

        bool ros_hardware_interface::enable_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(thumb_interface, "thumb", rq, rs);
        }

        bool ros_hardware_interface::disable_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(thumb_interface, "thumb", rq, rs);
        }

        bool ros_hardware_interface::reboot_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(thumb_interface, "thumb", rq, rs);
        }

        bool ros_hardware_interface::zero_thumb_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(thumb_interface, "thumb", true, rq, rs);
        }

        bool ros_hardware_interface::zero_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_thumb_unlocked(rq, rs);
        }

        bool ros_hardware_interface::enable_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
        }

        bool ros_hardware_interface::disable_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
        }

        bool ros_hardware_interface::reboot_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
        }

        bool ros_hardware_interface::zero_thumb_opposition_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(thumb_opposition_interface, "thumb_opposition", true, rq, rs);
        }

        bool ros_hardware_interface::zero_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_thumb_opposition_unlocked(rq, rs);
        }
        
        bool ros_hardware_interface::enable_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(index_interface, "index", rq, rs);
        }

        bool ros_hardware_interface::disable_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(index_interface, "index", rq, rs);
        }

        bool ros_hardware_interface::reboot_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(index_interface, "index", rq, rs);
        }

        bool ros_hardware_interface::zero_index_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(index_interface, "index", false, rq, rs);
        }

        bool ros_hardware_interface::zero_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_index_unlocked(rq, rs);
        }

        bool ros_hardware_interface::enable_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(middle_interface, "middle", rq, rs);
        }

        bool ros_hardware_interface::disable_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(middle_interface, "middle", rq, rs);
        }

        bool ros_hardware_interface::reboot_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(middle_interface, "middle", rq, rs);
        }

        bool ros_hardware_interface::zero_middle_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(middle_interface, "middle", false, rq, rs);
        }

        bool ros_hardware_interface::zero_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_middle_unlocked(rq, rs);
        }

        bool ros_hardware_interface::enable_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(ring_interface, "ring", rq, rs);
        }

        bool ros_hardware_interface::disable_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(ring_interface, "ring", rq, rs);
        }

        bool ros_hardware_interface::reboot_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(ring_interface, "ring", rq, rs);
        }

        bool ros_hardware_interface::zero_ring_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(ring_interface, "ring", false, rq, rs);
        }

        bool ros_hardware_interface::zero_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_ring_unlocked(rq, rs);
        }

        bool ros_hardware_interface::enable_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return enable_dynamixel(pinky_interface, "pinky", rq, rs);
        }

        bool ros_hardware_interface::disable_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return disable_dynamixel(pinky_interface, "pinky", rq, rs);
        }

        bool ros_hardware_interface::reboot_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return reboot_dynamixel(pinky_interface, "pinky", rq, rs);
        }

        bool ros_hardware_interface::zero_pinky_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            return zero_dynamixel(pinky_interface, "pinky", false, rq, rs);
        }

        bool ros_hardware_interface::zero_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);
            return zero_pinky_unlocked(rq, rs);
        }

        bool ros_hardware_interface::enable_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);

            enable_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
            if (!rs.success) return true;

            enable_dynamixel(thumb_interface, "thumb", rq, rs);
            if (!rs.success) return true;

            enable_dynamixel(index_interface, "index", rq, rs);
            if (!rs.success) return true;

            enable_dynamixel(middle_interface, "middle", rq, rs);
            if (!rs.success) return true;

            enable_dynamixel(ring_interface, "ring", rq, rs);
            if (!rs.success) return true;

            enable_dynamixel(pinky_interface, "pinky", rq, rs);
            if (!rs.success) return true;

            rs.message = "all enabled";
            return true;
        }

        bool ros_hardware_interface::disable_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);

            disable_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
            if (!rs.success) return true;

            disable_dynamixel(thumb_interface, "thumb", rq, rs);
            if (!rs.success) return true;

            disable_dynamixel(index_interface, "index", rq, rs);
            if (!rs.success) return true;

            disable_dynamixel(middle_interface, "middle", rq, rs);
            if (!rs.success) return true;

            disable_dynamixel(ring_interface, "ring", rq, rs);
            if (!rs.success) return true;

            disable_dynamixel(pinky_interface, "pinky", rq, rs);
            if (!rs.success) return true;

            rs.message = "all disabled";
            return true;
        }

        bool ros_hardware_interface::reboot_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);

            reboot_dynamixel(thumb_opposition_interface, "thumb_opposition", rq, rs);
            if (!rs.success) return true;

            reboot_dynamixel(thumb_interface, "thumb", rq, rs);
            if (!rs.success) return true;

            reboot_dynamixel(index_interface, "index", rq, rs);
            if (!rs.success) return true;

            reboot_dynamixel(middle_interface, "middle", rq, rs);
            if (!rs.success) return true;

            reboot_dynamixel(ring_interface, "ring", rq, rs);
            if (!rs.success) return true;

            reboot_dynamixel(pinky_interface, "pinky", rq, rs);
            if (!rs.success) return true;

            rs.message = "all rebooted";
            return true;
        }

        bool ros_hardware_interface::zero_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs)
        {
            std::lock_guard<std::mutex> lock(mutex);

            zero_thumb_opposition_unlocked(rq, rs);
            if (!rs.success) return true;

            zero_thumb_unlocked(rq, rs);
            if (!rs.success) return true;

            zero_index_unlocked(rq, rs);
            if (!rs.success) return true;

            zero_middle_unlocked(rq, rs);
            if (!rs.success) return true;

            zero_ring_unlocked(rq, rs);
            if (!rs.success) return true;

            zero_pinky_unlocked(rq, rs);
            if (!rs.success) return true;

            rs.message = "all zeroed";
            return true;
        }

} // new_dexterity

