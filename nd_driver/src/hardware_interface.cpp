#include <stdexcept>
#include <controller_manager/controller_manager.h>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <nd_ros_hardware_interface/ros_hardware_interface.hpp>
#include <chrono>
#include <cmath>

namespace asio = boost::asio;

namespace ndx_dynx = new_dexterity::dynamixel_protocol;

int main(int argc, char *argv[]) 
{
    ros::init(argc, argv, "nd_hardware_interface");

    ros::NodeHandle node_handle;

    asio::io_service io_service;

    asio::serial_port port(io_service);

    std::string usb_port;
    if (!node_handle.getParam("ndx_usb_port", usb_port))
    {
        throw std::logic_error("The interface requires the ndx_usb_port parameter to be set");
    }

    int baud;
    if (!node_handle.getParam("ndx_baud_rate", baud))
    {
    	throw std::logic_error("The interface requires the ndx_baud_rate parameter to be set");
    }

    try
    {
    	port.open(usb_port);
    }
    catch(boost::system::system_error& e)
    {
    	ROS_ERROR("USB Port %s",e.what());
    	exit(0);
    }

    port.set_option(asio::serial_port::baud_rate(baud));
    port.set_option(asio::serial_port::flow_control(asio::serial_port::flow_control::none));
    port.set_option(asio::serial_port::parity(asio::serial_port::parity::none));
    port.set_option(asio::serial_port::stop_bits(asio::serial_port::stop_bits::one));
    port.set_option(asio::serial_port::character_size(8));

    ndx_dynx::in_connection in_connection(port);
    ndx_dynx::out_connection out_connection(port);

    ndx_dynx::communication_interface thumb_opposition_interface(in_connection, out_connection, 1);
    ndx_dynx::communication_interface thumb_interface(in_connection, out_connection, 2);
    ndx_dynx::communication_interface index_interface(in_connection, out_connection, 3);
    ndx_dynx::communication_interface middle_interface(in_connection, out_connection, 4);
    ndx_dynx::communication_interface ring_interface(in_connection, out_connection, 5);
    ndx_dynx::communication_interface pinky_interface(in_connection, out_connection, 6);

    new_dexterity::ros_hardware_interface robot(node_handle, out_connection
                                                           , thumb_opposition_interface
                                                           , thumb_interface
                                                           , index_interface
                                                           , middle_interface
                                                           , ring_interface
                                                           , pinky_interface);

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });

    controller_manager::ControllerManager cm(&robot, node_handle);

    ros::AsyncSpinner spinner(1);
    ros::Rate rate(100);
    spinner.start();

    ros::Time previous = ros::Time::now();
    ros::Time now;
    ros::Duration duration;

    while (ros::ok()) 
    {
        now = ros::Time::now();
        duration = now - previous;

        while (!robot.read() && ros::ok());

        if (!ros::ok())
            break;

        cm.update(ros::Time::now(), duration);

        robot.write();

        previous = now;

        rate.sleep();
    }

    spinner.stop();
    io_service.stop();

    return 0;
}

