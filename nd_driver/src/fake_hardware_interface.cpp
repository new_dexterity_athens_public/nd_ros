#include <stdexcept>
#include <controller_manager/controller_manager.h>
#include <new_dexterity/communication_interface.hpp>
#include <nd_ros_hardware_interface/ros_hardware_interface.hpp>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <new_dexterity/utilities.hpp>
#include <new_dexterity/pipe.hpp>
#include <boost/asio.hpp>
#include <chrono>
#include <cmath>
#include <unistd.h>

namespace asio = boost::asio;
namespace ndx_dynx = new_dexterity::dynamixel_protocol;

int main(int argc, char *argv[]) 
{
    ros::init(argc, argv, "nd_hardware_interface");

    ros::NodeHandle node_handle;

    asio::io_service io_service;

    new_dexterity::pipe_endpoint endp1(io_service);
    new_dexterity::pipe_endpoint endp2(io_service);

    new_dexterity::pipe_endpoint endp3(io_service);
    new_dexterity::pipe_endpoint endp4(io_service);

    new_dexterity::create_pipe(endp1, endp2);
    new_dexterity::create_pipe(endp3, endp4);

    ndx_dynx::in_connection in(endp1);
    ndx_dynx::out_connection out(endp4);

    ndx_dynx::in_connection fake_in(endp3);
    ndx_dynx::out_connection fake_out(endp2);

    ndx_dynx::fake_dynamixel_driver fake_thumb_opposition(fake_in, fake_out, static_cast<std::uint8_t>(1));
    ndx_dynx::fake_dynamixel_driver fake_thumb(fake_in, fake_out, static_cast<std::uint8_t>(2));
    ndx_dynx::fake_dynamixel_driver fake_index(fake_in, fake_out, static_cast<std::uint8_t>(3));
    ndx_dynx::fake_dynamixel_driver fake_middle(fake_in, fake_out, static_cast<std::uint8_t>(4));
    ndx_dynx::fake_dynamixel_driver fake_ring(fake_in, fake_out, static_cast<std::uint8_t>(5));
    ndx_dynx::fake_dynamixel_driver fake_pinky(fake_in, fake_out, static_cast<std::uint8_t>(6));

    ndx_dynx::communication_interface thumb_opposition_interface(in, out, static_cast<std::uint8_t>(1));
    ndx_dynx::communication_interface thumb_interface(in, out, static_cast<std::uint8_t>(2));
    ndx_dynx::communication_interface index_interface(in, out, static_cast<std::uint8_t>(3));
    ndx_dynx::communication_interface middle_interface(in, out, static_cast<std::uint8_t>(4));
    ndx_dynx::communication_interface ring_interface(in, out, static_cast<std::uint8_t>(5));
    ndx_dynx::communication_interface pinky_interface(in, out, static_cast<std::uint8_t>(6));

    new_dexterity::ros_hardware_interface robot(node_handle, out,
            thumb_opposition_interface,
            thumb_interface,
            index_interface,
            middle_interface,
            ring_interface,
            pinky_interface);

    auto runner = std::async(std::launch::async, [&]{ io_service.run(); });

    controller_manager::ControllerManager cm(&robot, node_handle);

    ros::AsyncSpinner spinner(1);
    ros::Rate rate(100);
    spinner.start();

    ros::Time previous = ros::Time::now();
    ros::Time now;
    ros::Duration duration;

    while (ros::ok()) 
    {
        now = ros::Time::now();
        duration = now - previous;

        while (!robot.read() && ros::ok());

        if (!ros::ok())
            break;

        cm.update(ros::Time::now(), duration);

        robot.write();

        previous = now;

        rate.sleep();
    }

    spinner.stop();
    io_service.stop();
    endp1.close();
    endp2.close();
    endp3.close();
    endp4.close();

    return 0;
}

