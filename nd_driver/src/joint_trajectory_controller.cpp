#include <pluginlib/class_list_macros.h>

#include <nd_trajectory_interface/quintic_spline_segment.h>
#include <nd_joint_trajectory_controller/joint_trajectory_controller.h>
#include <nd_hardware_interface/posveleff_command_interface.hpp>

template <class State>
class HardwareInterfaceAdapter<nd_hardware_interface::PosVelEffJointInterface, State>
{
public:
  HardwareInterfaceAdapter() : joint_handles_ptr_(0) {}

  bool init(std::vector<nd_hardware_interface::PosVelEffJointHandle>& joint_handles, ros::NodeHandle& /*controller_nh*/)
  {
    // Store pointer to joint handles
    joint_handles_ptr_ = &joint_handles;

    return true;
  }

  void starting(const ros::Time& /*time*/) {}
  void stopping(const ros::Time& /*time*/) {}

  void updateCommand(const ros::Time&     /*time*/,
                     const ros::Duration& /*period*/,
                     const State&         desired_state,
                     const State&         /*state_error*/)
  {
    // Forward desired position to command
    const unsigned int n_joints = joint_handles_ptr_->size();
	for (unsigned int i = 0; i < n_joints; ++i)
    {
      (*joint_handles_ptr_)[i].setCommand(desired_state.position[i], desired_state.velocity[i], desired_state.effort[i]);
    }
  }

private:
  std::vector<nd_hardware_interface::PosVelEffJointHandle>* joint_handles_ptr_;
};

namespace nd_pos_vel_eff_controllers
{
    
    typedef joint_trajectory_controller::JointTrajectoryController<trajectory_interface::QuinticSplineSegment<double>, nd_hardware_interface::PosVelEffJointInterface>
        JointTrajectoryController;

}

PLUGINLIB_EXPORT_CLASS(nd_pos_vel_eff_controllers::JointTrajectoryController,   controller_interface::ControllerBase)


