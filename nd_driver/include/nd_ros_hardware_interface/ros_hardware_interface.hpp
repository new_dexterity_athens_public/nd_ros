#ifndef ND_ROS_HARDWARE_INTERFACE_ROS_HARDWARE_INTERFACE_H
#define ND_ROS_HARDWARE_INTERFACE_ROS_HARDWARE_INTERFACE_H

#include <nd_hardware_interface/posveleff_command_interface.hpp>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <thread>
#include <boost/optional.hpp>
#include <new_dexterity/dynamixel_protocol.hpp>
#include <std_srvs/Trigger.h>

namespace new_dexterity
{
    namespace joints
    {
        enum
        {
            THUMB_OPPOSITION_MOTOR,
            THUMB_MOTOR,
            INDEX_MOTOR,
            MIDDLE_MOTOR,
            RING_MOTOR,
            PINKY_MOTOR,
            THUMB_PROXIMAL,
            THUMB_DISTAL,
            INDEX_PROXIMAL,
            INDEX_MEDIAL,
            INDEX_DISTAL,
            MIDDLE_PROXIMAL,
            MIDDLE_MEDIAL,
            MIDDLE_DISTAL,
            RING_PROXIMAL,
            RING_MEDIAL,
            RING_DISTAL,
            PINKY_PROXIMAL,
            PINKY_MEDIAL,
            PINKY_DISTAL,
            TOTAL_JOINTS_COUNT
        };

        static constexpr size_t REAL_JOINTS_COUNT = THUMB_PROXIMAL - THUMB_OPPOSITION_MOTOR;
    }


    class ros_hardware_interface : public hardware_interface::RobotHW
    {

    public:
        ros_hardware_interface(ros::NodeHandle& node_handle, dynamixel_protocol::out_connection& out_connection
                                                           , dynamixel_protocol::communication_interface& thumb_opposition_interface
                                                           , dynamixel_protocol::communication_interface& thumb_interface
                                                           , dynamixel_protocol::communication_interface& index_interface
                                                           , dynamixel_protocol::communication_interface& middle_interface
                                                           , dynamixel_protocol::communication_interface& ring_interface
                                                           , dynamixel_protocol::communication_interface& pinky_interface);

	bool read();
	void write();

    private:
        void report_hardware_error_status(dynamixel_protocol::communication_interface& interface, ros::Publisher& publisher, std::string finger_name);

	bool read_info(dynamixel_protocol::communication_interface& interface, boost::optional<dynamixel_protocol::info_response> mresponse, ros::Publisher& hardware_status_publisher, ros::Publisher temperature_publisher,
                       std::string finger_name, double& position_feedback
	                                      , double& velocity_feedback
	                                      , double& torque_feedback);

	void prepare_cmd(dynamixel_protocol::sync_write<dynamixel_protocol::command_request>& sync_write, uint8_t id, std::string finger_name, double goal_position, double goal_velocity, double goal_torque);

        bool enable_dynamixel(dynamixel_protocol::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response);

        bool disable_dynamixel(dynamixel_protocol::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response);

        bool zero_dynamixel(dynamixel_protocol::communication_interface& interface, std::string finger_name, bool is_inverse,
                std_srvs::Trigger::Request const & rq, std_srvs::Trigger::Response & srv_response);

        bool reboot_dynamixel(dynamixel_protocol::communication_interface& interface, std::string finger_name, std_srvs::Trigger::Request const &, std_srvs::Trigger::Response & srv_response);

        bool enable_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_thumb_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_thumb(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool enable_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_thumb_opposition_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_thumb_opposition(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);
        
        bool enable_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_index_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_index(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool enable_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_middle_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_middle(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool enable_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_ring_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_ring(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool enable_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_pinky_unlocked(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_pinky(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool enable_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool disable_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool reboot_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);

        bool zero_all(std_srvs::Trigger::Request & rq, std_srvs::Trigger::Response & rs);


        hardware_interface::JointStateInterface joint_state_interface;
        nd_hardware_interface::PosVelEffJointInterface pos_vel_eff_joint_interface;

        ros::NodeHandle& node_handle;

        dynamixel_protocol::out_connection& out_connection;
        dynamixel_protocol::communication_interface& thumb_interface;
        dynamixel_protocol::communication_interface& thumb_opposition_interface;
        dynamixel_protocol::communication_interface& index_interface;
        dynamixel_protocol::communication_interface& middle_interface;
        dynamixel_protocol::communication_interface& ring_interface;
        dynamixel_protocol::communication_interface& pinky_interface;

        std::array<double, joints::TOTAL_JOINTS_COUNT> pos, vel, eff;
        std::array<double, joints::REAL_JOINTS_COUNT> cmd_pos, cmd_vel, cmd_eff;

        ros::Publisher thumb_hardware_status_publisher;
        ros::Publisher thumb_opposition_hardware_status_publisher;
        ros::Publisher index_hardware_status_publisher;
        ros::Publisher middle_hardware_status_publisher;
        ros::Publisher ring_hardware_status_publisher;
        ros::Publisher pinky_hardware_status_publisher;

        ros::Publisher thumb_temperature_publisher;
        ros::Publisher thumb_opposition_temperature_publisher;
        ros::Publisher index_temperature_publisher;
        ros::Publisher middle_temperature_publisher;
        ros::Publisher ring_temperature_publisher;
        ros::Publisher pinky_temperature_publisher;

        ros::ServiceServer thumb_reboot_service;
        ros::ServiceServer thumb_opposition_reboot_service;
        ros::ServiceServer index_reboot_service;
        ros::ServiceServer middle_reboot_service;
        ros::ServiceServer ring_reboot_service;
        ros::ServiceServer pinky_reboot_service;
        ros::ServiceServer all_reboot_service;

        ros::ServiceServer thumb_enable_service;
        ros::ServiceServer thumb_opposition_enable_service;
        ros::ServiceServer index_enable_service;
        ros::ServiceServer middle_enable_service;
        ros::ServiceServer ring_enable_service;
        ros::ServiceServer pinky_enable_service;
        ros::ServiceServer all_enable_service;

        ros::ServiceServer thumb_disable_service;
        ros::ServiceServer thumb_opposition_disable_service;
        ros::ServiceServer index_disable_service;
        ros::ServiceServer middle_disable_service;
        ros::ServiceServer ring_disable_service;
        ros::ServiceServer pinky_disable_service;
        ros::ServiceServer all_disable_service;

        ros::ServiceServer thumb_zero_service;
        ros::ServiceServer thumb_opposition_zero_service;
        ros::ServiceServer index_zero_service;
        ros::ServiceServer middle_zero_service;
        ros::ServiceServer ring_zero_service;
        ros::ServiceServer pinky_zero_service;
        ros::ServiceServer all_zero_service;

        std::mutex mutex;
    };

} // new_dexterity

#endif
