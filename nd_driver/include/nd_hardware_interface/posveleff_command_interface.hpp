#ifndef ND_POS_VEL_EFF_COMMAND_INTERFACE_H
#define ND_POS_VEL_EFF_COMMAND_INTERFACE_H

#include <cassert>
#include <string>
#include <hardware_interface/internal/hardware_resource_manager.h>
#include <hardware_interface/joint_state_interface.h>

namespace nd_hardware_interface
{

class PosVelEffJointHandle : public hardware_interface::JointStateHandle
{
public:
  PosVelEffJointHandle() : hardware_interface::JointStateHandle(), cmd_pos_(0), cmd_vel_(0), cmd_eff_(0) {}

  PosVelEffJointHandle(const hardware_interface::JointStateHandle& js, double* cmd_pos, double* cmd_vel, double* cmd_eff)
    : hardware_interface::JointStateHandle(js), cmd_pos_(cmd_pos), cmd_vel_(cmd_vel), cmd_eff_(cmd_eff)
  {
    if (!cmd_pos)
    {
      throw hardware_interface::HardwareInterfaceException("Cannot create handle '" + js.getName() + "'. Command position pointer is null.");
    }
    if (!cmd_vel)
    {
      throw hardware_interface::HardwareInterfaceException("Cannot create handle '" + js.getName() + "'. Command velocity  pointer is null.");
    }
    if (!cmd_eff)
    {
      throw hardware_interface::HardwareInterfaceException("Cannot create handle '" + js.getName() + "'. Command effort  pointer is null.");
    }
  }

  void setCommand(double cmd_pos, double cmd_vel, double cmd_eff)
  {
    setCommandPosition(cmd_pos);
    setCommandVelocity(cmd_vel);
    setCommandEffort(cmd_eff);
  }

  void setCommandPosition(double cmd_pos)     {assert(cmd_pos_); *cmd_pos_ = cmd_pos;}
  void setCommandVelocity(double cmd_vel)     {assert(cmd_vel_); *cmd_vel_ = cmd_vel;}
  void setCommandEffort(double cmd_eff)     {assert(cmd_eff_); *cmd_eff_ = cmd_eff;}

  double getCommandPosition()     const {assert(cmd_pos_); return *cmd_pos_;}
  double getCommandVelocity()     const {assert(cmd_vel_); return *cmd_vel_;}
  double getCommandEffort()     const {assert(cmd_eff_); return *cmd_eff_;}

private:
  double* cmd_pos_;
  double* cmd_vel_;
  double* cmd_eff_;
};

class PosVelEffJointInterface : public hardware_interface::HardwareResourceManager<PosVelEffJointHandle, hardware_interface::ClaimResources> {};

}

#endif
