#!/usr/bin/env python

#System imports
import os
import sys
import signal
import copy

# ROS imports
import roslib 
import rospy
import rospkg
import time
from functools import partial

#import RQT
from PyQt4 import QtCore, uic, QtGui

from math import sin, cos, pi, ceil

#ROS msg-srv impors
from rosgraph_msgs.msg import Log

from sensor_msgs.msg import JointState

from std_msgs.msg import String, UInt8

from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint

import rosnode

#Other

import yaml

#Load GUI files

qtCreatorFile = os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'nd_gui.ui')
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

qtACTPOSFRAME = os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'actuator_position_control_frame.ui')
Ui_Act_Pos_Frame, temp1 = uic.loadUiType(qtACTPOSFRAME)

qtACTSTATESFRAME = os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'actuator_states_frame.ui')
Ui_Act_States_Frame, temp2 = uic.loadUiType(qtACTSTATESFRAME)

qtACTTRAJFRAME = os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'actuator_trajectoryx.ui')
Ui_Act_Traj_Frame, temp3 = uic.loadUiType(qtACTTRAJFRAME)

RED_text_style = "background-color: #f2f1f0; color: red;"
GREEN_text_style = "background-color: #f2f1f0; color: green;"

server_names = ["/hardware_interface"]


def get_actuators_idx(names):
        idxs=[]
        if 'thumb_opposition_motor' in names:
            idxs.append(names.index('thumb_opposition_motor'))
        if 'thumb_motor' in names:
            idxs.append(names.index('thumb_motor'))
        if 'index_motor' in names:
            idxs.append(names.index('index_motor'))
        if 'middle_motor' in names:
            idxs.append(names.index('middle_motor'))
        if 'ring_motor'  in names:
            idxs.append(names.index('ring_motor'))
        if 'pinky_motor'  in names:
            idxs.append(names.index('pinky_motor'))
        return idxs


################################ Actuator States Frame CLASS ###############################################
class ActStatesFrame(QtGui.QFrame,Ui_Act_States_Frame):
    def __init__(self,centralwidget,height,mid, actuators):
                QtGui.QFrame.__init__(self,parent = centralwidget)
                Ui_Act_States_Frame.__init__(self)

                self.setupUi(self)
                self.move(720,height+75)

                motor = actuators[mid]
                mid = mid+1

                self.label.setText("Actuator #" + str(mid))

################################ Actuator Trajectory Frame CLASS ###############################################
class ActTrajFrame(QtGui.QFrame, Ui_Act_Traj_Frame):
        def __init__(self,centralwidget,data):
                QtGui.QFrame.__init__(self, parent = centralwidget)
                Ui_Act_Traj_Frame.__init__(self)

                self.setupUi(self)

                pi1 = QtGui.QTableWidgetItem()
                pi1.setText(str(data['positions'][0]))

                pi2 = QtGui.QTableWidgetItem()
                pi2.setText(str(data['positions'][1]))

                pi3 = QtGui.QTableWidgetItem()
                pi3.setText(str(data['positions'][2]))

                pi4 = QtGui.QTableWidgetItem()
                pi4.setText(str(data['positions'][3]))

                pi5 = QtGui.QTableWidgetItem()
                pi5.setText(str(data['positions'][4]))

                pi6 = QtGui.QTableWidgetItem()
                pi6.setText(str(data['positions'][5]))

                vi1 = QtGui.QTableWidgetItem()
                vi1.setText(str(data['velocities'][0]))

                vi2 = QtGui.QTableWidgetItem()
                vi2.setText(str(data['velocities'][1]))

                vi3 = QtGui.QTableWidgetItem()
                vi3.setText(str(data['velocities'][2]))

                vi4 = QtGui.QTableWidgetItem()
                vi4.setText(str(data['velocities'][3]))

                vi5 = QtGui.QTableWidgetItem()
                vi5.setText(str(data['velocities'][4]))

                vi6 = QtGui.QTableWidgetItem()
                vi6.setText(str(data['velocities'][5]))

                ti1 = QtGui.QTableWidgetItem()
                ti1.setText(str(data['torque'][0]))

                ti2 = QtGui.QTableWidgetItem()
                ti2.setText(str(data['torque'][1]))

                ti3 = QtGui.QTableWidgetItem()
                ti3.setText(str(data['torque'][2]))

                ti4 = QtGui.QTableWidgetItem()
                ti4.setText(str(data['torque'][3]))

                ti5 = QtGui.QTableWidgetItem()
                ti5.setText(str(data['torque'][4]))

                ti6 = QtGui.QTableWidgetItem()
                ti6.setText(str(data['torque'][5]))

                #self.tableWidget.setFocusPolicy(QtCore.Qt.NoFocus)

                self.tableWidget.setItem(0,0,pi1);
                self.tableWidget.setItem(0,1,pi2);
                self.tableWidget.setItem(0,2,pi3);
                self.tableWidget.setItem(0,3,pi4);
                self.tableWidget.setItem(0,4,pi5);
                self.tableWidget.setItem(0,5,pi6);

                self.tableWidget.setItem(1,0,vi1);
                self.tableWidget.setItem(1,1,vi2);
                self.tableWidget.setItem(1,2,vi3);
                self.tableWidget.setItem(1,3,vi4);
                self.tableWidget.setItem(1,4,vi5);
                self.tableWidget.setItem(1,5,vi6);

                self.tableWidget.setItem(2,0,ti1);
                self.tableWidget.setItem(2,1,ti2);
                self.tableWidget.setItem(2,2,ti3);
                self.tableWidget.setItem(2,3,ti4);
                self.tableWidget.setItem(2,4,ti5);
                self.tableWidget.setItem(2,5,ti6);
                
                labels = ["Thumb Opposition", "Thumb", "Index", "Middle", "Ring", "Pinky"]

                self.tableWidget.setHorizontalHeaderLabels(labels)
                self.tableWidget.resizeColumnsToContents()
                font = QtGui.QFont()
                font.setFamily("Ubuntu")
                font.setPointSize(8)
                self.tableWidget.horizontalHeader().setFont(font)
                self.tableWidget.horizontalHeader().setStretchLastSection(True)

                self.doubleSpinBox.setValue(data['secs'] + 0.000000001 * data['nsecs'])

                


################################ Actuator Position Commands Frame CLASS ####################################
class ActPosFrame(QtGui.QFrame,Ui_Act_Pos_Frame):
        def __init__(self,centralwidget,height,mid, actuators):
                QtGui.QFrame.__init__(self,parent = centralwidget)
                Ui_Act_Pos_Frame.__init__(self)

                motor = actuators[mid]

                self.setupUi(self)
                self.move(10,height)

                self.label.setText("Actuator #" + str(mid+1))
                self.joint_label.setText('Joint: ' + motor +' Motor')
                #self.max_velocity.setMaximum(480.0)
                self.max_torque.setMaximum(350.0)
                self.max_torque.setValue(250.0)

                self.command_perc_text.editingFinished.connect(self.ComPosTextFinished)
                self.command_perc_text.textEdited.connect(self.ComPosTextChanged)
                self.command_perc_text.selectionChanged.connect(self.ComPosTextChanged)

                self.text_edit = 0

                self.command_slider.valueChanged.connect(self.SliderMove)

                self.position_max = 300.0

                self.position_min = 0.0

                self.SliderMove()

        '''ComPosTextChanged'''
        def ComPosTextChanged(self):
                self.text_edit = 1
                try:
                        text_val = int(self.command_perc_text.text())

                        anglec = float(text_val)/100.0*(self.position_max-self.position_min)+self.position_min
                        anglec = round(anglec*1000.0)/1000.0
                
                        self.command_text.setText(str(anglec))

                except ValueError:
                        pass

        '''ComPosTextFinished'''
        def ComPosTextFinished(self):
                self.text_edit = 0
                try:
                        self.command_slider.setValue(int(self.command_perc_text.text()))
                except ValueError:
                        pass
                   
                self.SliderMove()

        '''SliderMove'''
        def SliderMove(self):
                slider_val = self.command_slider.value()

                self.command_perc_text.setText(str(slider_val))
                
                anglec = float(slider_val)/100.0*(self.position_max-self.position_min)+self.position_min
                anglec = round(anglec*1000.0)/1000.0
                
                self.command_text.setText(str(anglec))
        

################################ CLASS ###############################################
class NDControlApp(QtGui.QMainWindow, Ui_MainWindow):

        '''Constructor'''        
        def __init__(self):
                #GUI init
                QtGui.QMainWindow.__init__(self)
                Ui_MainWindow.__init__(self)
                self.setupUi(self)
                self.setFixedSize(self.size())

                self.icon_label.setPixmap(QtGui.QPixmap(os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'images','ndlogo.png')))

                self.server_init_status.setStyleSheet(RED_text_style)
                self.server_init_status.setText("Shutdown")

                self.save_traj_button.clicked.connect(self.HandleTrajectory) 
                self.add_empty_button.clicked.connect(self.HandleAddEmpty) 
                self.remove_selected_button.clicked.connect(self.HandleRemoveSelected) 
                self.clear_traj_button.clicked.connect(self.HandleClearTrajectory) 
                self.save_trajectory_button.clicked.connect(self.HandleSaveTrajectory) 
                self.load_trajectory_button.clicked.connect(self.HandleLoadTrajectory) 

                #self.reset_button.clicked.connect(self.ResetServiceCallback) 

                #MotorConfigs
                self.motors_num = 0
                self.motor_config = None
                self.motorsIDs = []

                #For Frames
                self.act_pos = []
                self.act_states = []
                self.num_act = 0
                self.height = 10
                self.count = 0
                self.height_gest = 458

                #HandleTrajectory
                self.handle_traj = False
                self.act_traj = []
                self.traj_height=10

                #Program appropriate definitions                
                self.server_init = False

                self.DestroyMotorFrames()
                self.frames_ok = False

                #Periodic Timer for "watchdog"
                self.ctimer = QtCore.QTimer()

                self.state = "SHUTDOWN"
                self.ctimer.timeout.connect(self.handle_state)

                #self.timeout.connect(self.ctimer, QtCore.SIGNAL("timeout()"), self.constantUpdate)
                self.wdt = 0.1
                self.ctimer.start(self.wdt*1000) #ms
        
                rospy.Subscriber("/joint_states", JointState, self.updateStates, queue_size=10000)

                rospy.Subscriber("/rosout", Log, self.updateROS, queue_size=10000)

                rospy.Subscriber("/hardware_interface/thumb_opposition_hardware_status", String, self.updateThumbOppositionStatus, queue_size=10000)
                rospy.Subscriber("/hardware_interface/thumb_hardware_status", String, self.updateThumbStatus, queue_size=10000)
                rospy.Subscriber("/hardware_interface/index_hardware_status", String, self.updateIndexStatus, queue_size=10000)
                rospy.Subscriber("/hardware_interface/middle_hardware_status", String, self.updateMiddleStatus, queue_size=10000)
                rospy.Subscriber("/hardware_interface/ring_hardware_status", String, self.updateRingStatus, queue_size=10000)
                rospy.Subscriber("/hardware_interface/pinky_hardware_status", String, self.updatePinkyStatus, queue_size=10000)

                rospy.Subscriber("/hardware_interface/thumb_opposition_temperature", UInt8, self.updateThumbOppositionTemperature, queue_size=10000)
                rospy.Subscriber("/hardware_interface/thumb_temperature", UInt8, self.updateThumbTemperature, queue_size=10000)
                rospy.Subscriber("/hardware_interface/index_temperature", UInt8, self.updateIndexTemperature, queue_size=10000)
                rospy.Subscriber("/hardware_interface/middle_temperature", UInt8, self.updateMiddleTemperature, queue_size=10000)
                rospy.Subscriber("/hardware_interface/ring_temperature", UInt8, self.updateRingTemperature, queue_size=10000)
                rospy.Subscriber("/hardware_interface/pinky_temperature", UInt8, self.updatePinkyTemperature, queue_size=10000)

                self.joint_trajectory_publisher  = rospy.Publisher("joint_trajectory_controller/command", JointTrajectory, queue_size=10000)

                #debug
                self.counter = 30
                #self.velocity_mode.setChecked(True)

                self.rosmsgtxt = ""
                self.newmsg = False
                self.newstate = False
                self.new_thumb_opposition_hardware_status_msg = False
                self.new_thumb_hardware_status_msg = False
                self.new_index_hardware_status_msg = False
                self.new_middle_hardware_status_msg = False
                self.new_ring_hardware_status_msg = False
                self.new_pinky_hardware_status_msg = False
                self.new_thumb_opposition_temperature_msg = False
                self.new_thumb_temperature_msg = False
                self.new_index_temperature_msg = False
                self.new_middle_temperature_msg = False
                self.new_ring_temperature_msg = False
                self.new_pinky_temperature_msg = False
                self.new_joint_states_msg = False

        def handle_state(self):
            if self.state == "SHUTDOWN":
                self.handle_state_SHUTDOWN()
            if self.state == "WAITING_FOR_SERVER":
                self.handle_state_WAITING_FOR_SERVER()
            if self.state == "FOUND_SERVER":
                self.handle_state_FOUND_SERVER()
            elif self.state == "WAITING_FOR_FIRST_MESSAGE":
                self.handle_state_WAITING_FOR_FIRST_MESSAGE()
            elif self.state == "CONSTRUCT_FRAMES":
                self.handle_state_CONSTRUCT_FRAMES()
            elif self.state == "PROCESSING":
                self.handle_state_PROCESSING()
            else:
                self.state == "SHUTDOWN"

        def appendTxtMsg(self, msg):
                self.rosmsgtxt += msg
                self.ros_msg_text.setText(self.rosmsgtxt)
                self.ros_msg_text.moveCursor(QtGui.QTextCursor.End)                

        def handle_state_SHUTDOWN(self):
                self.DestroyMotorFrames()
                self.motorsIDs = []
                self.hand_device_frame.hide()
                self.server_init_status.setStyleSheet(RED_text_style)
                self.server_init_status.setText("Shutdown")
                self.appendTxtMsg(" <br>= Server is down...please run the nd_driver node =<br> ")
                self.state = "WAITING_FOR_SERVER"

        def handle_state_WAITING_FOR_SERVER(self):
                nodelist = rosnode.get_node_names()

                for n in nodelist:
                    if n in server_names: 
                        self.state = "FOUND_SERVER"

        def handle_state_FOUND_SERVER(self):
                self.server_init_status.setStyleSheet("color: rgb(255, 125, 0);")
                self.server_init_status.setText("Initializing")
                self.appendTxtMsg(" <br>======= Server pending for initialization ======<br> ")
                self.state = "WAITING_FOR_FIRST_MESSAGE"


        def handle_state_WAITING_FOR_FIRST_MESSAGE(self):
                if self.new_joint_states_msg:
                    msg = self.joint_states_msg
                    joint_names = msg.name
                    self.actuators = ['Thumb Opposition', 'Thumb','Index','Middle','Ring', 'Pinky']

                    self.motorsIDs = get_actuators_idx(msg.name)        
                    self.motors_num = len(self.motorsIDs)
                    self.new_joint_states_msg = False
                    self.state = "CONSTRUCT_FRAMES"

        def handle_state_CONSTRUCT_FRAMES(self):
                self.CreateMotorFrames()
                self.hand_device_frame.show()
                self.server_init_status.setStyleSheet(GREEN_text_style)
                self.server_init_status.setText("Initialized")
                self.appendTxtMsg(" <br>=============== ServerReady =============<br> ")
                self.state = "PROCESSING"

        def handle_state_PROCESSING(self):
                self.counter += 1

                if self.counter == 30:
                    self.state = "SHUTDOWN"
                    return

                ci =  self.panel_tab.currentIndex()
                if ci == 1 and not self.handle_traj:
                        self.handle_traj = True
                        self.save_traj_button.setText("Trajectory Execute")
                                
                elif ci == 0 and self.handle_traj:
                        self.handle_traj = False
                        self.save_traj_button.setText("Trajectory Save Point")

                if self.newmsg:
                    self.ros_msg_text.setText(self.rosmsgtxt)
                    self.ros_msg_text.moveCursor(QtGui.QTextCursor.End)                
                    self.newmsg =False

                if self.new_joint_states_msg:
                    self.handleJointStatesMsg(self.joint_states_msg)
                    self.new_joint_states_msg = False

                if self.frames_ok and self.new_thumb_opposition_hardware_status_msg:
                    self.handleThumbOppositionStatusMsg(self.thumb_opposition_hardware_status_msg)
                    self.new_thumb_opposition_hardware_status_msg = False

                if self.frames_ok and self.new_thumb_opposition_temperature_msg:
                    self.handleThumbOppositionTemperatureMsg(self.thumb_opposition_temperature_msg)
                    self.new_thumb_opposition_temperature_msg = False

                if self.frames_ok and self.new_thumb_hardware_status_msg:
                    self.handleThumbStatusMsg(self.thumb_hardware_status_msg)
                    self.new_thumb_hardware_status_msg = False

                if self.frames_ok and self.new_thumb_temperature_msg:
                    self.handleThumbTemperatureMsg(self.thumb_temperature_msg)
                    self.new_thumb_temperature_msg = False

                if self.frames_ok and self.new_index_hardware_status_msg:
                    self.handleIndexStatusMsg(self.index_hardware_status_msg)
                    self.new_index_hardware_status_msg = False

                if self.frames_ok and self.new_index_temperature_msg:
                    self.handleIndexTemperatureMsg(self.index_temperature_msg)
                    self.new_index_temperature_msg = False

                if self.frames_ok and self.new_middle_hardware_status_msg:
                    self.handleMiddleStatusMsg(self.middle_hardware_status_msg)
                    self.new_middle_hardware_status_msg = False

                if self.frames_ok and self.new_middle_temperature_msg:
                    self.handleMiddleTemperatureMsg(self.middle_temperature_msg)
                    self.new_middle_temperature_msg = False

                if self.frames_ok and self.new_ring_hardware_status_msg:
                    self.handleRingStatusMsg(self.ring_hardware_status_msg)
                    self.new_ring_hardware_status_msg = False

                if self.frames_ok and self.new_ring_temperature_msg:
                    self.handleRingTemperatureMsg(self.ring_temperature_msg)
                    self.new_ring_temperature_msg = False

                if self.frames_ok and self.new_pinky_hardware_status_msg:
                    self.handlePinkyStatusMsg(self.pinky_hardware_status_msg)
                    self.new_pinky_hardware_status_msg = False

                if self.frames_ok and self.new_pinky_temperature_msg:
                    self.handlePinkyTemperatureMsg(self.pinky_temperature_msg)
                    self.new_pinky_temperature_msg = False


        def updateThumbOppositionStatus(self, msg):
            self.thumb_opposition_hardware_status_msg = msg
            self.new_thumb_opposition_hardware_status_msg = True

        def updateThumbOppositionTemperature(self, msg):
            self.thumb_opposition_temperature_msg = msg
            self.new_thumb_opposition_temperature_msg = True

        def updateThumbStatus(self, msg):
            self.thumb_hardware_status_msg = msg
            self.new_thumb_hardware_status_msg = True

        def updateThumbTemperature(self, msg):
            self.thumb_temperature_msg = msg
            self.new_thumb_temperature_msg = True

        def updateIndexStatus(self, msg):
            self.index_hardware_status_msg = msg
            self.new_index_hardware_status_msg = True

        def updateIndexTemperature(self, msg):
            self.index_temperature_msg = msg
            self.new_index_temperature_msg = True

        def updateMiddleStatus(self, msg):
            self.middle_hardware_status_msg = msg
            self.new_middle_hardware_status_msg = True

        def updateMiddleTemperature(self, msg):
            self.middle_temperature_msg = msg
            self.new_middle_temperature_msg = True

        def updateRingStatus(self, msg):
            self.ring_hardware_status_msg = msg
            self.new_ring_hardware_status_msg = True

        def updateRingTemperature(self, msg):
            self.ring_temperature_msg = msg
            self.new_ring_temperature_msg = True

        def updatePinkyStatus(self, msg):
            self.pinky_hardware_status_msg = msg
            self.new_pinky_hardware_status_msg = True

        def updatePinkyTemperature(self, msg):
            self.pinky_temperature_msg = msg
            self.new_pinky_temperature_msg = True

        def HandleClearTrajectory(self):
            self.listWidget.clear()

        def HandleAddEmpty(self):
            traj = ActTrajFrame(self.listWidget, 
                    {'positions': 
                          [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     'velocities' : 
                          [0.0, 0.0, 0.0, 0.0, 0.0, 0.0], 
                     'torque':
                          [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                     'secs' : 0 ,
                     'nsecs' : 0})

            item = QtGui.QListWidgetItem()
            item.setSizeHint(traj.size())
            
            selected = self.listWidget.currentItem()
            index = self.listWidget.indexFromItem(selected)

            self.listWidget.insertItem(index.row() + 1, item)
            self.listWidget.setItemWidget(item, traj)


        def HandleRemoveSelected(self):
            item = self.listWidget.currentItem()
            index = self.listWidget.indexFromItem(item)
            model = self.listWidget.model()
            model.removeRow(index.row())
            
        def HandleSaveTrajectory(self):
            filename = QtGui.QFileDialog.getSaveFileName()
            msg = self.createTrajectoryMsg()
            with open(filename, "w") as traj_file:
                traj_file.write(str(msg))

        def HandleLoadTrajectory(self):
            filename = QtGui.QFileDialog.getOpenFileName()
            msg = JointTrajectory()
            self.listWidget.clear()
            with open(filename, "r") as traj_file:
                msg_str = traj_file.read()
                data = yaml.load(msg_str)
                for point in data['points']:
                   traj = ActTrajFrame(self.listWidget, 
                      {'positions': map(lambda x: x / 0.0174533, point['positions']),
                       'velocities': map(lambda x: x / 0.0174533, point['velocities']),
                       'torque': point['effort'],
                       'secs' : point['time_from_start']['secs'],
                       'nsecs' : point['time_from_start']['nsecs']})
                   item = QtGui.QListWidgetItem(self.listWidget)
                   item.setSizeHint(traj.size())
                   self.listWidget.addItem(item)
                   self.listWidget.setItemWidget(item, traj)
                   self.traj_height=self.traj_height+160
                        
            
        def createTrajectoryMsg(self):
            msg = JointTrajectory()
            msg.joint_names = ["thumb_opposition_motor", "thumb_motor", "index_motor", "middle_motor", "ring_motor", "pinky_motor"]

            for i in xrange(self.listWidget.count()):
                table = self.listWidget.itemWidget(self.listWidget.item(i))
                point = JointTrajectoryPoint()

                point.positions = [0.0174533 * float(table.tableWidget.item(0,0).text()),
                           0.0174533 * float(table.tableWidget.item(0,1).text()),
                           0.0174533 * float(table.tableWidget.item(0,2).text()),
                           0.0174533 * float(table.tableWidget.item(0,3).text()),
                           0.0174533 * float(table.tableWidget.item(0,4).text()),
                           0.0174533 * float(table.tableWidget.item(0,5).text())]
                
                point.velocities = [0.0174533 * float(table.tableWidget.item(1,0).text()),
                            0.0174533 * float(table.tableWidget.item(1,1).text()),
                            0.0174533 * float(table.tableWidget.item(1,2).text()),
                            0.0174533 * float(table.tableWidget.item(1,3).text()),
                            0.0174533 * float(table.tableWidget.item(1,4).text()),
                            0.0174533 * float(table.tableWidget.item(1,5).text())]
                
                point.effort = [float(table.tableWidget.item(2,0).text()),
                        float(table.tableWidget.item(2,1).text()),
                        float(table.tableWidget.item(2,2).text()),
                        float(table.tableWidget.item(2,3).text()),
                        float(table.tableWidget.item(2,4).text()),
                        float(table.tableWidget.item(2,5).text())]
                
                point.time_from_start = rospy.Duration(table.doubleSpinBox.value())
            
                msg.points.append(point)

            return msg


        def handleThumbOppositionStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[0].status_text.setText(str(msg.data))

        def handleThumbOppositionTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[0].temperature_text.setText(str(msg.data))

        def handleThumbStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[1].status_text.setText(str(msg.data))

        def handleThumbTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[1].temperature_text.setText(str(msg.data))

        def handleIndexStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[2].status_text.setText(str(msg.data))

        def handleIndexTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[2].temperature_text.setText(str(msg.data))

        def handleMiddleStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[3].status_text.setText(str(msg.data))

        def handleMiddleTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[3].temperature_text.setText(str(msg.data))

        def handleRingStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[4].status_text.setText(str(msg.data))

        def handleRingTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[4].temperature_text.setText(str(msg.data))

        def handlePinkyStatusMsg(self, msg):
            if self.frames_ok:
                self.act_states[5].status_text.setText(str(msg.data))

        def handlePinkyTemperatureMsg(self, msg):
            if self.frames_ok:
                self.act_states[5].temperature_text.setText(str(msg.data))

        def handleJointStatesMsg(self, msg):
                joint_names = msg.name
                
                if (self.num_act==self.motors_num) and not self.frames_ok:
                        for x in range(0,self.num_act):
                                mid = self.motorsIDs[x];
                                anglec = msg.position[mid]*180.0/pi
                                slider_val = int((anglec - self.act_pos[x].position_min)*100.0/(self.act_pos[x].position_max-self.act_pos[x].position_min))
                                self.act_pos[x].command_slider.setValue(slider_val)
                                
                        self.frames_ok = True                
                text_edit = 0
                for x in range(0,self.num_act):
                        mid = self.motorsIDs[x]

                        posx = msg.position[mid]*180.0/pi
                        velx = msg.velocity[mid]*180.0/pi

                        pos = round(posx*100.0)/100.0
                        vel = round(velx*100.0)/100.0
                        torq = round(msg.effort[mid]*100.0)/100.0


                        self.act_states[x].position_text.setText(str(pos))
                        self.act_states[x].velocity_text.setText(str(vel))
                        self.act_states[x].torque_text.setText(str(torq))
                                

                        text_edit = text_edit + self.act_pos[x].text_edit


                if ((self.motors_num==self.num_act) and (self.motors_num>0) and (text_edit==0) and self.frames_ok):
                        self.publish_commands()
        

        def createCommandDict(self):
                data = {}
                data['positions'] = [float(self.act_pos[0].command_text.text()),
                                   float(self.act_pos[1].command_text.text()),
                                   float(self.act_pos[2].command_text.text()),
                                   float(self.act_pos[3].command_text.text()),
                                   float(self.act_pos[4].command_text.text()),
                                   float(self.act_pos[5].command_text.text())]
                data['torque']= [float(self.act_pos[0].max_torque.value()),
                                float(self.act_pos[1].max_torque.value()),
                                float(self.act_pos[2].max_torque.value()),
                                float(self.act_pos[3].max_torque.value()),
                                float(self.act_pos[4].max_torque.value()),
                                float(self.act_pos[5].max_torque.value())]

                return data

        '''Publish Commands Function'''
        def publish_commands(self):
            if not self.handle_traj and self.frames_ok:
                point = JointTrajectoryPoint()
                data = self.createCommandDict()
                point.positions = list(map(lambda x : 0.0174533 * x, data['positions']))
                point.effort = data['torque']
                point.time_from_start = rospy.Duration(0.1)
                msg = JointTrajectory()
                msg.joint_names = ["thumb_opposition_motor", "thumb_motor", "index_motor", "middle_motor", "ring_motor", "pinky_motor"]
                msg.points = [point]
                self.joint_trajectory_publisher.publish(msg)

        '''Actuators MSG ROS Callback'''    
        def updateStates(self,msg):
            self.counter = 0
            self.new_joint_states_msg = True
            self.joint_states_msg = msg


        def HandleTrajectory(self):
                if self.handle_traj: #Execute
                        if self.listWidget.count() == 0:
                                message = '<br><font color="orange"> [WARN] There are no saved trajectory points for execution </font>'
                                self.rosmsgtxt += message
                                self.newmsg=True
                                return
                        else:
                            publisher = rospy.Publisher("joint_trajectory_controller/command", JointTrajectory, queue_size=1000)

                            msg = self.createTrajectoryMsg()

                            publisher.publish(msg) 
                else:        #Save
                            
                    data = self.createCommandDict()
                    traj = ActTrajFrame(self.listWidget, 
                            {'positions': data['positions'],
                             'velocities': 
                                  [0.0,0.0,0.0,0.0,0.0,0.0],
                             'torque': data['torque'],
                             'secs' : 0,
                             'nsecs' : 0})

                    item = QtGui.QListWidgetItem(self.listWidget)
                    item.setSizeHint(traj.size())
                    self.listWidget.addItem(item)
                    self.listWidget.setItemWidget(item, traj)
                    self.traj_height=self.traj_height+160
                                
                        

        def updateROS(self,msg):
                node_name = msg.name

                if node_name in server_names: 
                        startmsg = '<br><font color="'
                        if msg.level==2:
                                startmsg+='black"> [INFO] ['
                        elif msg.level==4:
                                startmsg+='orange"> [WARN] ['
                        else:
                                startmsg+='red"> [ERROR] ['

                        message = startmsg + str(msg.header.stamp.to_sec())+'] ' + msg.msg +  '</font>'

                        self.rosmsgtxt += message
                        self.newmsg=True


        '''Create Motor Frame Windows based on motor configuration'''
        def CreateMotorFrames(self):
                upper = 6;
                for x in range(0,upper):
                        self.act_states.append(ActStatesFrame(self.hand_device_frame,self.height,x, self.actuators))
                        self.act_pos.append(ActPosFrame(self.motor_tab,self.height,x, self.actuators))

                        self.act_pos[x].show()
                        self.act_states[x].show()

                        self.height = self.height + self.act_pos[x].height() + 2

                        self.num_act = self.num_act+1

                        if (self.height+350>790):
                                self.panel_tab.setFixedSize(680,self.height+50)
                                #self.setFixedSize(1300, self.height+300)
                                self.setFixedSize(1500, self.height+300)
                                #self.title.move(820,self.height+300-50)

                self.hand_device_frame.show()
        
        '''Destroy Motor Frame Windows if none device selected'''
        def DestroyMotorFrames(self):
                for actpos in self.act_pos:
                        actpos.close()
                        actpos = None

                for actstates in self.act_states:
                        actstates.close()

                self.act_pos = []
                self.act_states = []
                self.height = 10
                self.num_act = 0

                self.panel_tab.setFixedSize(680,540)
                self.setFixedSize(1500, 790)
                #self.title.move(820,713)

                self.hand_device_frame.hide()
                self.frames_ok = False

############################# MAIN ###########################################s
if __name__ == "__main__":        
        app = QtGui.QApplication(sys.argv)
    
        rospy.init_node('nd_control_ui')

        window = NDControlApp()

        app.setWindowIcon(QtGui.QIcon(os.path.join(rospkg.RosPack().get_path('nd_ui'), 'resources', 'images','ndlogo_icon.png')))

        signal.signal(signal.SIGINT, lambda *a: app.quit())
        window.show()
   
sys.exit(app.exec_())
