# New Dexterity Hand ROS Packages

## Description

New Dexterity Hand ROS stack. 

* Maintainer: Liarokapis Alexandros <liarokapis.v@gmail.com>

## 1. Overview

The New Dexterity Hand uses Robotis' XH430-V350-R dynamixels and exposes their communication directly.
As a result one may use any of Robotis' software in order to control and configure the Dynamixels.

- The default baud rate used for communication is 3000000
- The id of the thumb opposition dynamixel is 1
- The id of the thumb dynamixel is 2
- The id of the index dynamixel is 3
- The id of the middle dynamixel is 4
- The id of the ring dynamixel is 5
- The id of the pinky dynamixel is 6

All other dynamixel settings are left to the default values.

Our ROS stack Consists of the following packages:

- The `nd_description` package. Contains the meshes and the URDFs used for visualization.
- The `nd_driver` package. Contains a custom `joint_trajectory_controller` accepting position and torque-limit commands. It's goal is to
either reach the specified position or the specified current (torque) limit. Also provides a fake driver node that works with dynamixel mockers as well
as an actual driver exposing the custom controller.
- The `nd_ui` package. Contains a GUI that allows the control of the hand via sliders as well as a trajectory editor.

All of the packages use Robotis' Protocol 2.0 in order to communicate with the Dynamixels. 
We also provide the [`nd_api`](https://gitlab.com/new_dexterity_athens_public/nd_api.git) package which 
exposes a high-level modern C++ alternative to Robotis' Dynamixel SDK. In fact we use this in the
implementation of the driver.

## 2. Prerequisites

Before installing this software make sure that you have a computer with Linux OS and ROS pre-installed as well as an Ethernet port.

One must also install our [`nd_api`](https://gitlab.com/new_dexterity_athens_public/nd_api.git) communication library. Take note that
our C++ communication library also provides a valid package.xml file so one can simply clone in their catkin workspace and run an isolated build
using either `catkin_make_isolated` or catkin tools' `catkin build`. We are using this approach in the below installation instructions for
convenience.

## 3. Installation 

The below bash commands install both our C++ communication library and our ros package using catkin tools' `catkin build`
One can always seperately install our C++ communication library using the instructions on our repository and just ignore
the cloning below.

```bash
mkdir -p nd_catkin_ws/src
cd nd_catkin_ws/src
git clone https://gitlab.com/new_dexterity_athens_public/nd_api.git
git clone https://gitlab.com/new_dexterity_athens_public/nd_ros.git
cd .. 
source /opt/ros/{distro}/setup.bash
sudo rosdep init
rosdep update
rosdep install --from-paths . -i -y
sudo apt install python-catkin-tools
catkin build
```

## 4. Running the driver

### Setting up the usb port

The usb port must first be set to raw mode.
Additionally one may control the port's latency timer in
order to reduce the time that the linux driver waits every read.

```bash
sudo stty -F /dev/ttyUSB0 raw
echo 1 | sudo tee /sys/bus/usb-serial/devices/ttyUSB0/latency_timer
```

### Running the launch file

```bash
source /opt/ros/{distro}/setup.bash
source nd_catkin_ws/devel/setup.bash
roslaunch nd_driver hand_interface_driver.launch
```

The default usb port is the `/dev/ttyUSB0`. You can change the port with the
`ndx_usb_port` argument. 

The default baud rate used to communicate with the dynamixels is 3000000. You can change the baud rate with the
`ndx_baud_rate` argument.

Rviz can be activated by using the `rviz:=true` argument. Due to the lack of sensors
and the under-actuated nature of the hand, the visualization assumes that the actuators
are zeroed in the natural position.

### Optional

A virtual hand driver can be used if no ndx hand is available: 
```bash 
roslaunch nd_driver fake_hand_interface_driver.launch 
```

## 5. Topics

- `/hardware_interface/thumb_opposition_temperature` 

Retrieves the hardware error status of the thumb opposition dynamixel.

- `/hardware_interface/thumb_opposition_hardware_status` 

Retrieves the temperature in Celcius of the thumb opposition dynamixel.

- `/hardware_interface/thumb_hardware_status` 

Retrieves the hardware error status of the thumb dynamixel.

- `/hardware_interface/thumb_temperature` 

Retrieves the temperature in Celcius of the thumb dynamixel.

- `/hardware_interface/index_hardware_status` 

 Retrieves the hardware error status of the index dynamixel.

- `/hardware_interface/index_temperature` 

Retrieves the temperature in Celcius of the index dynamixel.

- `/hardware_interface/middle_hardware_status`

Retrieves the hardware error status of the middle dynamixel.

- `/hardware_interface/middle_temperature` 

Retrieves the temperature in Celcius of the middle dynamixel.

- `/hardware_interface/pinky_hardware_status` 

Retrieves the hardware error status of the pinky dynamixel.

- `/hardware_interface/pinky_temperature` 

Retrieves the temperature in Celcius of the pinky dynamixel.

- `/hardware_interface/ring_hardware_status`

Retrieves the hardware error status of the ring dynamixel.

- `/hardware_interface/ring_temperature` 

Retrieves the temperature in Celcius of the ring dynamixel.

Our driver also exposes all topics described in the [`joint_trajectory_controller`](http://wiki.ros.org/joint_trajectory_controller) wiki,
the only difference is that in the `/joint_trajectory_controller/command` one may also specify effort which will act as a current (torque) limit in mA and will
be interpolated the same way that position does.

## 6. Services

- `/hardware_interface/disable_all`
- `/hardware_interface/disable_index`
- `/hardware_interface/disable_middle`
- `/hardware_interface/disable_pinky`
- `/hardware_interface/disable_ring`
- `/hardware_interface/disable_thumb`
- `/hardware_interface/disable_thumb_opposition`

Disables the dynamixels' torque enable control value. The dynamixels
won't be able to exert any force or accept any commands and will slowly be returned to their zero-position.

- `/hardware_interface/enable_all`
- `/hardware_interface/enable_index`
- `/hardware_interface/enable_middle`
- `/hardware_interface/enable_pinky`
- `/hardware_interface/enable_ring`
- `/hardware_interface/enable_thumb`
- `/hardware_interface/enable_thumb_opposition`

Enables the dynamixels' torque enable control value. The dynamixels
will start exerting force and accepting commands. 
The dynamixels start disabled by default, one should manually enable 
the dynamixels before sending any commands.

- `/hardware_interface/get_loggers`
- `/hardware_interface/reboot_all`
- `/hardware_interface/reboot_index`
- `/hardware_interface/reboot_middle`
- `/hardware_interface/reboot_pinky`
- `/hardware_interface/reboot_ring`
- `/hardware_interface/reboot_thumb`
- `/hardware_interface/reboot_thumb_opposition`

Reboots the dynamixels. Useful when a hardware error alert occurs as the 
dynamixels will disable their torque by default and require a reboot in order
to turn it on again.

- `/hardware_interface/zero_all`
- `/hardware_interface/zero_index`
- `/hardware_interface/zero_middle`
- `/hardware_interface/zero_pinky`
- `/hardware_interface/zero_ring`
- `/hardware_interface/zero_thumb`
- `/hardware_interface/zero_thumb_opposition`

Sets the current position as the new zero position. Disables torque
so that any commands won't send the fingers to an undesired position. This along with the disable/enable services may be 
used to calibrate the hand. One simple calibration procedure is to close the fingers slightly, disable the torque and let
the springs return the fingers to their original position. Then one can zero the dynamixels in that position in order to
set the natural zero position.

## 5. Running the GUI

After running the driver, simply run:

```bash
rosrun nd_ui nd_gui.py
```

This will activate the GUI which provides actuator control via sliders as well as a trajectory editor.

One should check that the dynamixels report a plausible position, otherwise one may need to set the zero position using the above services.

After enabling the dynamixels with the enable services above the hand will start accepting commands.
One may save the hand's current pose as a trajectory point and edit the points in the trajectory editor tab. Users must still
set the correct `time_from_start` for each point saved. Trajectories can be saved and loaded into/from files.

